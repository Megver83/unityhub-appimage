# UnityHub AppImage
[![Latest Release](https://gitlab.com/Megver83/unityhub-appimage/-/badges/release.svg)](https://gitlab.com/Megver83/unityhub-appimage/-/releases)

This repository hosts a [pkg2appimage](https://github.com/AppImage/pkg2appimage) recipe to build an UnityHub AppImage from the official deb source. In combination with GitLab CI and a scheduled pipeline, this project automatically builds and uploads AppImages on new UnityHub versions in the releases section of this GitLab repository.

## Desktop integration

UnityHub uses custom URL handler for, for example, logging in. If you go ahead and run the bare AppImage, your browser might not be able to open Unity URIs.
Solution? the `appimaged` daemon implementation of [go-appimage](https://github.com/probonopd/go-appimage). Read [here](https://github.com/probonopd/go-appimage/blob/master/src/appimaged/README.md) to learn how to use it.

## Diff with upstream functions.sh

* Download the latest version of packages available, rather than the oldest ones ([#516](https://github.com/AppImage/pkg2appimage/issues/516)).
* Get the package version from the deb source repo ([patch](https://gitlab.com/Megver83/unityhub-appimage/-/commit/c02055cab81c8242ce1c3ce72d7bc46ca15adc14#970914fd40d9a6825856296d9ead4120f934572b)).
* Fix build on `appimagecrafters/appimage-builder` image ([#517](https://github.com/AppImage/pkg2appimage/issues/517)) ([patch](https://gitlab.com/Megver83/unityhub-appimage/-/commit/deba80024aa8dc637c3a50a64405da5d34315009)).
