#!/usr/bin/env bash
# Check for new UnityHub releases not released in https://gitlab.com/Megver83/unityhub-appimage
# Useful docs:
# https://docs.gitlab.com/ee/api/index.html
# https://docs.gitlab.com/ee/api/releases/index.html#list-releases
set -e

DATA='https://hub.unity3d.com/linux/repos/deb/dists/stable/main/binary-amd64/Packages.gz'
GITLAB_API='https://gitlab.com/api/v4'
GITLAB_ID=35953384

# Gets the latest versions
UPSTREAM_VERSION="$(curl -sL "$DATA" | zgrep -A1 ^'Package: unityhub'$ | tail -n1 | cut -c10-)"
[[ "${#UPSTREAM_VERSION}" -gt 0 ]]
APPIMAGE_VERSION="$(curl -sL "$GITLAB_API/projects/$GITLAB_ID/releases" | jq -r '.[0]["tag_name"]')"
[[ "${#APPIMAGE_VERSION}" -gt 0 ]]

if [[ "$UPSTREAM_VERSION" == "$APPIMAGE_VERSION" ]]; then
    echo "Up-to-date"
else
    echo "Versions difference found"
    echo "Upstream: $UPSTREAM_VERSION"
    echo "AppImage: $APPIMAGE_VERSION"
fi
